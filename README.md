Virginia Tech Metadata Repository for Shibboleth
================================================

This project provides for version control of Shibboleth metadata needed for IdP integrations with Virginia Tech and hosted
services outside InCommon or other Higher Ed federations.


#!/usr/bin/env python3

import argparse
import sys

from os.path import basename
from urllib.request import urlopen
from xml.dom import Node
from xml.dom.minidom import parse

REG_BY_INC = 'http://id.incommon.org/category/registered-by-incommon'


def isElement(node, name):
  return hasattr(node, 'tagName') and name == node.tagName

def hasText(node, value):
  for child in node.childNodes:
    if Node.TEXT_NODE == child.nodeType and child.data == value:
      return True
  return False

def isRegistered(entity):
  for attr in entity.getElementsByTagName('saml:Attribute'):
    for child in attr.childNodes:
      if isElement(child, 'saml:AttributeValue') and hasText(child, REG_BY_INC):
        return True
  return False

def isSP(entity):
  return len(entity.getElementsByTagName('SPSSODescriptor')) > 0


parser = argparse.ArgumentParser(description='Produces a lean version of InCommon metadata suitable for a test SP.')
parser.add_argument('--metadata_url',
                    default='http://md.incommon.org/InCommon/InCommon-metadata.xml',
                    help='InCommon metadata URL (default http://md.incommon.org/InCommon/InCommon-metadata.xml)')
parser.add_argument('--outfile',
                    default='/tmp/InCommon-lean.xml',
                    help='Path to output file (default /tmp/InCommon-lean.xml')
args = parser.parse_args()
print('Downloading and parsing metadata from {}'.format(args.metadata_url))
metadata = parse(urlopen(args.metadata_url))
count = 0
firstChildren = metadata.documentElement.childNodes
for child in firstChildren:
  if isElement(child, 'ds:Signature'):
    metadata.documentElement.removeChild(child)
  if isElement(child, 'EntityDescriptor'):
    if isRegistered(child) and isSP(child):
      pass
    else:
      count += 1
      print('Removing ' + child.attributes['entityID'].value)
      metadata.documentElement.removeChild(child)

print('Removed {} entities from document. {} remain'.format(count, len(metadata.documentElement.childNodes)))
print('Writing file to {}'.format(args.outfile))
f = open(args.outfile, 'w')
try:
  metadata.writexml(f, indent='', addindent='  ', encoding='utf-8')
finally:
  f.close()
